<?php

/**
 *
 * Extension for Contao Open Source CMS (contao.org)
 *
 * Copyright (c) 2016-2018 POSTYOU
 *
 * @package
 * @author  Mario Gienapp
 * @link    http://www.postyou.de
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

namespace postyou;

class ModuleNewsListFacebook extends \ModuleNewsList
{
    public function __construct($objModule, $strColumn = 'main')
    {
        $GLOBALS['TL_JAVASCRIPT']['video'] = 'system/modules/contao-facebook-connector_basic/assets/js/video.js';
        parent::__construct($objModule, $strColumn);
    }

    /**
     * Parse an item and return it as string
     *
     * @param \NewsModel $objArticle
     * @param boolean    $blnAddArchive
     * @param string     $strClass
     * @param integer    $intCount
     *
     * @return string
     */
    protected function parseArticle($objArticle, $blnAddArchive=false, $strClass='', $intCount=0)
    {
        /** @var \PageModel $objPage */
        global $objPage;

        /** @var \FrontendTemplate|object $objTemplate */
        $objTemplate = new \FrontendTemplate($this->news_template);
        $objTemplate->setData($objArticle->row());

        $objTemplate->class = (($objArticle->cssClass != '') ? ' ' . $objArticle->cssClass : '') . $strClass;
        $objTemplate->newsHeadline = $objArticle->headline !== '-' ? $objArticle->headline : '';
        $objTemplate->subHeadline = $objArticle->subheadline;
        $objTemplate->hasSubHeadline = $objArticle->subheadline ? true : false;
        if ($objArticle->isFacebookPost) {
            $objTemplate->linkHeadline = '<a href="'.$objArticle->facebookLink.'" target="_blank">'.$objArticle->headline.'</a>';
        } else {
            $objTemplate->linkHeadline = $this->generateLink($objArticle->headline, $objArticle, $blnAddArchive);
        }

        $objTemplate->more = $this->generateLink($GLOBALS['TL_LANG']['MSC']['more'], $objArticle, $blnAddArchive, true);
        $objTemplate->facebookLinkHref = $objArticle->facebookLink;
        $objTemplate->videoLink = $objArticle->videoLink;
        $objTemplate->link = $this->generateNewsUrl($objArticle, $blnAddArchive);
        $objTemplate->archive = $objArticle->getRelated('pid');
        $objTemplate->count = $intCount; // see #5708
        $objTemplate->text = '';
        $objTemplate->hasText = false;
        $objTemplate->hasTeaser = false;
        $objTemplate->cssID = 'news-'.$objArticle->id;
        $objTemplate->isFacebookPost = $objArticle->isFacebookPost;
        $objTemplate->videoThumb = $objArticle->videoThumb;

        // Clean the RTE output
        if ($objArticle->teaser != '') {
            $objTemplate->hasTeaser = true;

            if ($objPage->outputFormat == 'xhtml') {
                $objTemplate->teaser = \StringUtil::toXhtml($objArticle->teaser);
            } else {
                $objTemplate->teaser = \StringUtil::toHtml5($objArticle->teaser);
            }

            $objTemplate->teaser = \StringUtil::encodeEmail($objTemplate->teaser);
        }

        //auf null setzen, wird nur bei gekuerztem Text gesetzt
        $objTemplate->facebookLink = null;

        //Textlaenge kuerzen
        if (!empty($this->messageLength) && (strlen($objTemplate->teaser) > $this->messageLength)) {
            if ($objArticle->isFacebookPost) {
                $objTemplate->teaser = \StringUtil::substr($objTemplate->teaser, $this->messageLength,
        ' ...');
                \System::loadLanguageFile('tl_facebook_posts');
                $objTemplate->facebookLink = '<a target="_blank" href="'.$objArticle->facebookLink.'">'.($post['type'] == 'video' ? $GLOBALS['TL_LANG']['tl_facebook_posts']['videoLinkText'] : $GLOBALS['TL_LANG']['tl_facebook_posts']['facebookLinkText']).'</a>';
            }
        } else {
            if ($this->showFacebookLinkAlways && $objArticle->isFacebookPost) {
                $objTemplate->facebookLink = '<a target="_blank" href="'.$objArticle->facebookLink.'">'.($post['type'] == 'video' ? $GLOBALS['TL_LANG']['tl_facebook_posts']['videoLinkText'] : $GLOBALS['TL_LANG']['tl_facebook_posts']['facebookLinkText']).'</a>';
            }
        }

        //Link Erkennung
        if ($objArticle->isFacebookPost) {
            $objTemplate->teaser = \FbConnectorHelper::autolink($objTemplate->teaser, array('target' => '_blank'));
        }

        //Hash Tag Entfernen
        if ($objArticle->removeHashTag) {
            $objTemplate->teaser = \FbConnectorHelper::removeHashTag($objTemplate->teaser);
        }



        // Display the "read more" button for external/article links
        if ($objArticle->source != 'default' && $objArticle->source != 'facebook') {
            $objTemplate->text = true;
            $objTemplate->hasText = true;
        }

        // Compile the news text
        else {
            $id = $objArticle->id;

            $objTemplate->text = function () use ($id) {
                $strText = '';
                $objElement = \ContentModel::findPublishedByPidAndTable($id, 'tl_news');

                if ($objElement !== null) {
                    while ($objElement->next()) {
                        $strText .= $this->getContentElement($objElement->current());
                    }
                }

                return $strText;
            };

            $objTemplate->hasText = (\ContentModel::countPublishedByPidAndTable($objArticle->id, 'tl_news') > 0);
        }

        $arrMeta = $this->getMetaFields($objArticle);

        // Add the meta information
        $objTemplate->date = $arrMeta['date'];
        $objTemplate->hasMetaFields = !empty($arrMeta);
        $objTemplate->numberOfComments = $arrMeta['ccount'];
        $objTemplate->commentCount = $arrMeta['comments'];
        $objTemplate->timestamp = $objArticle->date;
        $objTemplate->author = $arrMeta['author'];
        $objTemplate->datetime = date('Y-m-d\TH:i:sP', $objArticle->date);

        $objTemplate->addImage = false;

        $imagePathCount = null;
        $objTemplate->beforeStyle = null;

        // Add an image
        if ($objArticle->addImage) {
            if ($objArticle->singleSRC != '') {
                $objModel = \FilesModel::findByUuid($objArticle->singleSRC);

                if ($objModel === null) {
                    if (!\Validator::isUuid($objArticle->singleSRC)) {
                        $objTemplate->text = '<p class="error">'.$GLOBALS['TL_LANG']['ERR']['version2format'].'</p>';
                    }
                } elseif (is_file(TL_ROOT . '/' . $objModel->path)) {
                    // Do not override the field now that we have a model registry (see #6303)
                    $arrArticle = $objArticle->row();

                    // Override the default image size
                    if ($this->imgSize != '') {
                        $size = deserialize($this->imgSize);

                        if ($size[0] > 0 || $size[1] > 0 || is_numeric($size[2])) {
                            $arrArticle['size'] = $this->imgSize;
                        }
                    }
                    $arrArticle['singleSRC'] = $objModel->path;
                    $this->addImageToTemplate($objTemplate, $arrArticle);
                    $objTemplate->addImage = true;
                }
            }
            if ($objArticle->multiSRC != '') {
                $multiSRC = unserialize($objArticle->multiSRC);

                $images = array();

                if (is_array($multiSRC)) {
                    foreach ($multiSRC as $uuid) {
                        $size = unserialize(!empty($objArticle->size) ? $objArticle->size : $this->imgSize);
                        $path = \FilesModel::findByUuid($uuid)->path;
                        $tempArray = array();
                        if (!empty($path)) {
                            if (is_array($size)) {
                                $picture = \Picture::create(\FilesModel::findByUuid($uuid)->path, $size)->getTemplateData();
                                $tempArray['picture'] = $picture;
                            }
                            $tempArray['imagePath'] = $path;
                        } else {
                            continue;
                        }

                        $images[] = $tempArray;
                    }
                    $objTemplate->images = $images;

                    $imagePathCount = count($images);

                    $objTemplate->images = $images;
                    $objTemplate->addImage = true;
                    $objTemplate->href = $objArticle->fullsize;
                }
            }
        } elseif (!empty($objArticle->imageSrcFacebook)) {
            $objTemplate->imageSrcFacebook = $objArticle->imageSrcFacebook;
            $imagePathCount = count(unserialize($objArticle->imageSrcFacebook));
        }

        $cssID = 'news-'.$objArticle->id;

        if ($imagePathCount > 1) {
            $objTemplate->beforeStyle = '<style>#'.$cssID.' .image_container a.cboxElement:after {
                            content: "+'.$imagePathCount.'";
                        }</style>';
        }

        $objTemplate->enclosure = array();

        // Add enclosures
        if ($objArticle->addEnclosure) {
            $this->addEnclosuresToTemplate($objTemplate, $objArticle->row());
        }

        // HOOK: add custom logic
        if (isset($GLOBALS['TL_HOOKS']['parseArticles']) && is_array($GLOBALS['TL_HOOKS']['parseArticles'])) {
            foreach ($GLOBALS['TL_HOOKS']['parseArticles'] as $callback) {
                $this->import($callback[0]);
                $this->{$callback[0]}->{$callback[1]}($objTemplate, $objArticle->row(), $this);
            }
        }

        return $objTemplate->parse();
    }
}
