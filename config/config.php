<?php

/**
 *
 * Extension for Contao Open Source CMS (contao.org)
 *
 * Copyright (c) 2016-2018 POSTYOU
 *
 * @package
 * @author  Mario Gienapp
 * @link    http://www.postyou.de
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

if (! defined('TL_ROOT')) {
    die('You cannot access this file directly!');
}

//Hooks
$GLOBALS['TL_HOOKS']['executePreActions'][] = array('BackendAjaxHookPro', 'executePreActions');


// BE Module
$GLOBALS['BE_MOD']['Facebook']['Facebook-Seiten']['login'] = array(
        'postyou\FbBackendWrapperPro',
        'login'
);

$GLOBALS['BE_MOD']['content']['news']['deleteOnFacebook'] = array(
        'postyou\FbBackendWrapper',
        'onDeleteFacebook'
);


$GLOBALS['TL_CTE']['Facebook']['facebook_galleries'] = 'postyou\FacebookGallery';
$GLOBALS['TL_CTE']['Facebook']['facebook_event_list'] = 'postyou\FacebookEventList';

$GLOBALS['FE_MOD']['news']['newslist_facebook'] = 'postyou\ModuleNewsListFacebook';
$GLOBALS['FE_MOD']['events']['eventreader_facebook'] = 'postyou\ModuleEventReaderFacebook';

//Hooks
$GLOBALS['TL_HOOKS']['newsListFetchItems'][] = array('NewsListFacebookHook', 'filterFacebookPosts');
$GLOBALS['TL_HOOKS']['createFacebookModel'][] = array('CreateFacebookModelHook', 'createModelDependingNewsmoduleSetting');
$GLOBALS['TL_HOOKS']['getAllEvents'][] = array('EventListFacebookHook', 'formatTeaser');
