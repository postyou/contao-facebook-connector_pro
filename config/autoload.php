<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2018 Leo Feyer
 *
 * @license LGPL-3.0+
 */


/**
 * Register the namespaces
 */
ClassLoader::addNamespaces(array
(
	'postyou',
));


/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
	// Classes
	'postyou\FbBackendWrapperPro'               => 'system/modules/contao-facebook-connector_pro/classes/FbBackendWrapperPro.php',
	'FbConnectorEvents'                         => 'system/modules/contao-facebook-connector_pro/classes/FbConnectorEvents.php',
	'postyou\FbConnectorGalleryGet'             => 'system/modules/contao-facebook-connector_pro/classes/FbConnectorGalleryGet.php',
	'postyou\FbConnectorPhpSdk'                 => 'system/modules/contao-facebook-connector_pro/classes/FbConnectorPhpSdk.php',
	'postyou\FbConnectorPostPublish'            => 'system/modules/contao-facebook-connector_pro/classes/FbConnectorPostPublish.php',

	// Elements
	'postyou\FacebookEventList'                 => 'system/modules/contao-facebook-connector_pro/elements/FacebookEventList.php',
	'postyou\FacebookGallery'                   => 'system/modules/contao-facebook-connector_pro/elements/FacebookGallery.php',

	// Hooks
	'postyou\BackendAjaxHookPro'                => 'system/modules/contao-facebook-connector_pro/hooks/BackendAjaxHookPro.php',
	'CreateFacebookModelHook'                   => 'system/modules/contao-facebook-connector_pro/hooks/CreateFacebookModelHook.php',
	'postyou\EventListFacebookHook'             => 'system/modules/contao-facebook-connector_pro/hooks/EventListFacebookHook.php',
	'NewsListFacebookHook'                      => 'system/modules/contao-facebook-connector_pro/hooks/NewsListFacebookHook.php',

	// Models
	'postyou\FacebookContaoCalendarEventsModel' => 'system/modules/contao-facebook-connector_pro/models/FacebookContaoCalendarEventsModel.php',
	'postyou\FacebookContaoNewsModel'           => 'system/modules/contao-facebook-connector_pro/models/FacebookContaoNewsModel.php',
	'postyou\FacebookEventsModel'               => 'system/modules/contao-facebook-connector_pro/models/FacebookEventsModel.php',
	'postyou\FacebookGalleriesModel'            => 'system/modules/contao-facebook-connector_pro/models/FacebookGalleriesModel.php',
	'postyou\FacebookPostDeleteListModel'       => 'system/modules/contao-facebook-connector_pro/models/FacebookPostDeleteListModel.php',

	// Modules
	'postyou\ModuleEventReaderFacebook'         => 'system/modules/contao-facebook-connector_pro/modules/ModuleEventReaderFacebook.php',
	'postyou\ModuleNewsListFacebook'            => 'system/modules/contao-facebook-connector_pro/modules/ModuleNewsListFacebook.php',
));


/**
 * Register the templates
 */
TemplateLoader::addFiles(array
(
	'ce_facebook_events'   => 'system/modules/contao-facebook-connector_pro/templates',
	'ce_facebook_gallery'  => 'system/modules/contao-facebook-connector_pro/templates',
	'event_facebook'       => 'system/modules/contao-facebook-connector_pro/templates',
	'event_facebook_full'  => 'system/modules/contao-facebook-connector_pro/templates',
	'mod_facebook_events'  => 'system/modules/contao-facebook-connector_pro/templates',
	'mod_facebook_gallery' => 'system/modules/contao-facebook-connector_pro/templates',
	'news_facebook'        => 'system/modules/contao-facebook-connector_pro/templates',
));
