<?php

/**
 *
 * Extension for Contao Open Source CMS (contao.org)
 *
 * Copyright (c) 2016-2018 POSTYOU
 *
 * @package
 * @author  Mario Gienapp
 * @link    http://www.postyou.de
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

use Contao\DataContainer;
use Contao\Dbafs;
use postyou\FacebookPostsDeleteListModel;

$GLOBALS['TL_DCA']['tl_facebook_galleries'] = array(
    'config' => array(
        'dataContainer' => 'Table',
        'ptable' => 'tl_facebook_sites',
        'switchToEdit' => true,
        // 'enableVersioning' => true,
        'sql' => array(
            'keys' => array(
                'id' => 'primary',
                'pid' => 'index'
            )
        ),
        'notCreatable' => true,
        'notCopyable' => true,
        'ondelete_callback' => array(
            array(
                'tl_facebook_galleries',
                'onDelete'
            )
        ),
        'onsubmit_callback' => array(
            array(
                'tl_facebook_galleries',
                'onSubmit'
            )
        )
    ),
    'list' => array(
        'sorting' => array(
            'mode' => 4,
            'fields' => array(
                "updated_time, title"
            ),
            'panelLayout' => ('filter;search,sort;limit'),
            'disableGrouping' => true,
            'headerFields' => array(
                'title'
            ),
            'child_record_callback' => array(
                'tl_facebook_galleries',
                'showInList'
            )
        ),
        'label' => array(
            'fields' => array(
                'title'
            ),
            'format' => '%s',
            'showColumns' => true,
            'label_callback' => array(
                'tl_facebook_galleries',
                'labelCallback'
            )
        ),
        'global_operations' => array(
            'all' => array(
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset()" accesskey="e"'
            )
        ),
        'operations' => array( // operationen der einträge
            'edit' => array(
                'label' => &$GLOBALS['TL_LANG']['tl_facebook_galleries']['edit'],
                'href' => 'act=edit',
                'icon' => 'edit.gif'
            ),
            'copy' => array(
                'label' => &$GLOBALS['TL_LANG']['tl_facebook_galleries']['copy'],
                'href' => 'act=paste&amp;mode=copy',
                'icon' => 'copy.gif',
                'attributes' => 'onclick="Backend.getScrollOffset()"'
            ),
            'delete' => array(
                'label' => &$GLOBALS['TL_LANG']['tl_facebook_galleries']['delete'],
                'href' => 'act=delete',
                'icon' => 'delete.gif',
                'attributes' => 'onclick="if(!confirm(\'' .
                     $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] .
                     '\'))return false;Backend.getScrollOffset()"'
            ),
            'show' => array(
                'label' => &$GLOBALS['TL_LANG']['tl_facebook_galleries']['show'],
                'href' => 'act=show',
                'icon' => 'show.gif',
                'attributes' => 'style="margin-right:3px"'
            )
        )
    ),
    // "edit"=>array(),
    'palettes' => array(
        'default' => '{title_legend},title, multiSRC;'
    ),
    'subpalettes' => array(),
    'fields' => array(
        'id' => array(
            'sql' => "int(10) unsigned NOT NULL auto_increment"
        ),
        'pid' => array(
            'relation' => array(
                'table' => "tl_facebook_sites",
                "field" => "id",
                'type' => 'belongsTo',
                'load' => 'lazy'
            ),
            'sql' => "int(10) unsigned NOT NULL"
        ),
        'tstamp' => array(
            'sql' => "int(10) unsigned NOT NULL default '0'"
        ),
        'updated_time' => array(
            // 'label' => &$GLOBALS['TL_LANG']['tl_facebook_posts']['updated_time'],
            'sql' => "int(10) unsigned NOT NULL default '0'",
            // 'sorting' => true,
            'flag' => 12
        ),
        'created_time' => array(
            'label' => &$GLOBALS['TL_LANG']['tl_facebook_galleries']['created_time'],
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'sorting' => true,
            'flag' => 12
        ),
        'title' => array(
            'label' => &$GLOBALS['TL_LANG']['tl_facebook_galleries']['title'],
            // 'filter' => true,
            'search' => true,
            'sorting' => true,
            // 'exclude' => true,
            'inputType' => 'text',
            'eval' => array(
                // 'mandatory' => true,
                'maxlength' => 255,
                'readonly' => true
            ),
            'sql' => "varchar(255) NOT NULL default ''"
        ),
        'albumId' => array(
            'exclude' => true,
            'sql' => "varchar(255) NOT NULL default ''"
        ),
        'multiSRC' => array(
            'label' => &$GLOBALS['TL_LANG']['tl_facebook_galleries']['multiSRC'],
            'exclude' => true,
            'inputType' => 'fileTree',
            'eval' => array(
                'multiple' => true,
                'fieldType' => 'checkbox',
                'files' => true,
                'extensions' => \Config::get('validImageTypes'),
                'isGallery' => true,
                'tl_class' => 'clr'
            ),
            'sql' => "blob NULL"
        ),
        'orderSRC' => array(
            'label' => &$GLOBALS['TL_LANG']['tl_content']['orderSRC'],
            'sql' => "blob NULL"
        )
    )
);

class tl_facebook_galleries extends \Backend
{
    public function onSubmit($dc)
    {
        if (empty($dc->activeRecord->created_time)) {
            $this->Database->prepare("UPDATE tl_facebook_galleries SET created_time=? WHERE id=?")->execute(
                time(), $dc->id);
        }

        $this->Database->prepare("UPDATE tl_facebook_galleries SET updated_time=? WHERE id=?")->execute(
            time(), $dc->id);
    }

    public function onLoad($dc)
    {
        return;
    }

    public function onDelete($dc)
    {
        $facebookGalleryModel = \FacebookGalleriesModel::findById($dc->activeRecord->id);
        $folderName = str_replace(' ', '_', $facebookGalleryModel->albumId.'_'.$facebookGalleryModel->title);
        $path = \Config::get('uploadPath') . '/facebook/albums/' . $folderName;

        $folder = new \Folder($path);
        $folder->purge();
        $folder->delete();

        return;
    }

    public function onVersion($dc)
    {
        return;
    }

    public function labelCallback($row, $label, $dc, $args)
    {
        return $args;
    }

    public function showInList($arrRow)
    {
        $date = new DateTime();
        $date->setTimestamp($arrRow['updated_time']);
        $icon = '';
        $thumbnail = '';
        if (! empty($arrRow['icon'])) {
            $icon = '<img src="' . $arrRow['icon'] . '" />';
        }
        if (! empty($arrRow['thumbnailPath'])) {
            $filesize = filesize(TL_ROOT . '/' . $arrRow['thumbnailPath']);
            if ($filesize && $filesize > 0) {
                $thumbnail = '<br><img src="' . $arrRow['thumbnailPath'] . '"/>';
            }
        }

        return $icon . ' <em>' . $date->format($GLOBALS['TL_CONFIG']['datimFormat']) . '</em> ' .
             $arrRow['title'] . $thumbnail;
    }
}
