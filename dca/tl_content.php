<?php

/**
 *
 * Extension for Contao Open Source CMS (contao.org)
 *
 * Copyright (c) 2016-2018 POSTYOU
 *
 * @package
 * @author  Mario Gienapp
 * @link    http://www.postyou.de
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

$GLOBALS['TL_DCA']['tl_content']['palettes']['facebook_galleries'] = '{type_legend},type,headline,headlineOptn,facebookGallery,sizeFacebook,imagemargin,perRow,perPage,numberOfItems';

$GLOBALS['TL_DCA']['tl_content']['palettes']['facebook_event_list'] = '{type_legend},type,headline,headlineOptn,facebookSites,perPage,maxEvents,messageLength,removeHashTag';

$GLOBALS['TL_DCA']['tl_content']['fields']['maxEvents'] = array(
    'label' => &$GLOBALS['TL_LANG']['tl_content']['maxEvents'],
    'exclude' => true,
    'inputType' => 'text',
    'eval' => array(
        'tl_class' => 'clr',
        'default' => 0,
        'rgxp'=>'natural'
    ),
    'sql' => "int(10) unsigned NOT NULL default '0'"
);

$GLOBALS['TL_DCA']['tl_content']['fields']['facebookGallery'] = array(
    'label' => &$GLOBALS['TL_LANG']['tl_content']['facebookGallery'],
    'exclude' => true,
    'inputType' => 'radio',
    'foreignKey' => 'tl_facebook_galleries.title',
    'relation' => array('load' => 'lazy', 'type' => 'hasOne'),
    'eval' => array(
        'tl_class' => 'clr',
        'mandatory' => true
    ),
    'options_callback' => array('tl_content_facebook', 'getGalleryOptions'),
    'sql' => "int(10) unsigned NULL"

);

if (!class_exists('tl_content_facebook')) {
  class tl_content_facebook extends \tl_content
  {
      public function getGalleryOptions(DataContainer $dc)
      {
          $galleries = \FacebookGalleriesModel::findAll();
          $options = array();

          $lastPid = null;
          $facebookSiteTitle = null;
          while ($galleries->next()) {
              if (empty($lastPid) || $lastPid !== $galleries->current->pid) {
                  $facebookSiteTitle = \FacebookSitesModel::findById($galleries->current()->pid)->title;
              }

              $options[$galleries->current()->id] = $galleries->current()->title.' - <em>'.$facebookSiteTitle.'</em>';

              $lastPid = $galleries->current->pid;
          }

          return $options;
      }
  }
}
