<?php

/**
 *
 * Extension for Contao Open Source CMS (contao.org)
 *
 * Copyright (c) 2016-2018 POSTYOU
 *
 * @package
 * @author  Mario Gienapp
 * @link    http://www.postyou.de
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

namespace postyou;

class FbBackendWrapperPro
{
    public function onDeleteFacebook()
    {
        $newsModel = \NewsModel::findById(\Input::get('id'));

        if (!empty($newsModel) && !empty($newsModel->facebookPostId)) {
            $facebookPostDeleteListModel = new FacebookPostDeleteListModel();
            $facebookPostDeleteListModel->postId = $newsModel->facebookPostId;
            $facebookPostDeleteListModel->pid = $newsModel->facebookSitePid;
            $facebookPostDeleteListModel->save();
        }

        $redirectUrl = \Controller::addToUrl('&act=delete', true, array('key'));
        \Controller::redirect($redirectUrl);
    }

    public function login()
    {
        $fbConnector = \FbConnector::getInstance(
            array(
                'connectionType' => ConnectionType::POST_PUBLISH
            ));
        $fbConnector->login();

        $session = \Session::getInstance();

        $url = \Controller::addToUrl($session->get('redirectUri'), true, array(
            'key', 'code'
        ));

        \Controller::redirect($url);
    }
}
