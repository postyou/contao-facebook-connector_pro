<?php

/**
 *
 * Extension for Contao Open Source CMS (contao.org)
 *
 * Copyright (c) 2016-2018 POSTYOU
 *
 * @package
 * @author  Mario Gienapp
 * @link    http://www.postyou.de
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

namespace postyou;

use Facebook\Facebook;

class FbConnectorPhpSdk extends \FbConnector
{
    protected $fb;

    protected $accesstoken;

    protected function initFacebookPhpSdk()
    {
        $this->fb = new Facebook(
            [
                'app_id' => parent::getAppID(),
                'app_secret' => parent::getAppSecret(),
                'default_graph_version' => parent::getVersion(),
                'persistent_data_handler' => new ContaoPersistentDataHandler(),
                'default_access_token' => parent::getAppID() . '|' . parent::getAppSecret()
            ]);
    }

    public function login()
    {
        session_start();
        $_SESSION['BE_DATA']['FBRLH_state'] = $_GET['state'];

        $helper = $this->fb->getRedirectLoginHelper();

        try {
            $accessToken = $helper->getAccessToken();
            // var_dump($accessToken);
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit();
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit();
        }

        if (isset($accessToken)) {
            // Logged in!
            $_SESSION['facebook_access_token'] = (string) $accessToken;
            $this->accesstoken = (string) $accessToken;
        } else {
            if ($helper->getError()) {
                header('HTTP/1.0 401 Unauthorized');
                echo "Error: " . $helper->getError() . "\n";
                echo "Error Code: " . $helper->getErrorCode() . "\n";
                echo "Error Reason: " . $helper->getErrorReason() . "\n";
                echo "Error Description: " . $helper->getErrorDescription() . "\n";
            } else {
                header('HTTP/1.0 400 Bad Request');
                echo 'Bad request';
            }
            exit();
        }
    }

    protected function getPageAccessTokenFromFacebookAlias($facebookAlias)
    {
        $siteId = $this->fb->get($facebookAlias)->getDecodedBody()['id'];

        $facebookResponse = $this->fb->get('me/accounts', $_SESSION['facebook_access_token']);
        $accountPages = $facebookResponse->getDecodedBody()['data'];
        // echo '<pre>';
        // // var_dump($accountPages);
        // // var_dump($this->fb->get($facebookAlias)->getDecodedBody());
        // echo '</pre>';
        $pageAccessToken = '';

        // $data = $accountPages['data'];
        foreach ($accountPages as $page) {
            if ($page['id'] === $siteId) {
                // var_dump($page);
                $pageAccessToken = $page['access_token'];
                break;
            }
        }

        return $pageAccessToken;
    }
}
