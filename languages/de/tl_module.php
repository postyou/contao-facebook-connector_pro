<?php

/**
 *
 * Extension for Contao Open Source CMS (contao.org)
 *
 * Copyright (c) 2016-2018 POSTYOU
 *
 * @package
 * @author  Mario Gienapp
 * @link    http://www.postyou.de
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

$GLOBALS['TL_LANG']['tl_module']['hideFacebookNews'][0] = 'Facebook News ausblenden';
$GLOBALS['TL_LANG']['tl_module']['hideFacebookNews'][1] = 'Inhalte von Facebook werden in der Liste nicht ausgegeben.';
$GLOBALS['TL_LANG']['tl_module']['hideFacebookEvents'][0] = 'Facebook Events ausblenden';
$GLOBALS['TL_LANG']['tl_module']['hideFacebookEvents'][1] = 'Veranstaltungen von Facebook werden in der Liste nicht ausgegeben.';
