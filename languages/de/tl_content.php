<?php

/**
 *
 * Extension for Contao Open Source CMS (contao.org)
 *
 * Copyright (c) 2016-2018 POSTYOU
 *
 * @package
 * @author  Mario Gienapp
 * @link    http://www.postyou.de
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

$GLOBALS['TL_LANG']['tl_content']['facebookGallery'][0] = 'Facebook Galerie';
$GLOBALS['TL_LANG']['tl_content']['facebookGallery'][1] = 'W&auml;hlen Sie die Facebookgalerie aus, die auf der Webseite ausgegeben werden soll.';
$GLOBALS['TL_LANG']['tl_content']['facebook_event_list'][0] = 'Facebook Veranstaltungen';
$GLOBALS['TL_LANG']['tl_content']['facebook_event_list'][1] = 'W&auml;hlen Sie die Facebookveranstaltungen aus, die auf der Webseite ausgegeben werden soll.';
$GLOBALS['TL_LANG']['tl_content']['maxEvents'][0] = 'Maximalanzahl Elemente';
$GLOBALS['TL_LANG']['tl_content']['maxEvents'][1] = 'Maximale Anzahl der Elemente die ausgegeben werden sollen.';