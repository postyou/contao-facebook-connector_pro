<?php

/**
 *
 * Extension for Contao Open Source CMS (contao.org)
 *
 * Copyright (c) 2016-2018 POSTYOU
 *
 * @package
 * @author  Mario Gienapp
 * @link    http://www.postyou.de
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

$GLOBALS['TL_LANG']['tl_calendar_events']['isFacebookEvent'][0] = 'Auf Facebook ver&ouml;ffentlichen';
$GLOBALS['TL_LANG']['tl_calendar_events']['isFacebookEvent'][1] = 'Diese Veranstaltung wird auf Facebook ver&ouml;ffentlicht.';
