<?php

/**
 *
 * Extension for Contao Open Source CMS (contao.org)
 *
 * Copyright (c) 2016-2018 POSTYOU
 *
 * @package
 * @author  Mario Gienapp
 * @link    http://www.postyou.de
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

$GLOBALS['TL_LANG']['CTE']['facebook_galleries'][0] = 'Facebook Galerie';
$GLOBALS['TL_LANG']['CTE']['facebook_event_list'][0] = 'Facebook Veranstaltungen Auflistung';
$GLOBALS['TL_LANG']['FMD']['eventreader_facebook'][0] = 'Facebook Veranstaltungen Eventleser';
$GLOBALS['TL_LANG']['FMD']['newslist_facebook'][0] = 'Facebook News Nachrichtenliste';
